#include "header.h"


int main(){

    piece * game_board = game_board_init();

    char * display_board = display_board_init();

    update_display_board(display_board, game_board);
    
    system("clear");

    show_display_board(display_board);

    main_game_loop(display_board, game_board);

    return 0;
}

