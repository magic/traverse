#ifndef MAIN_HEADER
#define MAIN_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct{
    int player;
    char letter;
}piece;


typedef struct{
    int * data;
    int size;
}options;

/**
 * @fn game_board_init
 * @author Samson Grice
 * @version 1.0
 * @date Mon 30 Mar 2020 12:55:11 UTC
 * @brief a fonction that initilizes the game board
 *
 * @param (the number of players) <- todo
 *
 * @return the game board with the correct pieces in play;
 * */
piece * game_board_init();



/**
 * @fn display_boad_init
 * @author Samson Grice
 * @version 1.0
 * @date Mon 30 Mar 2020 12:56:56 UTC
 * @brief a fonction that initilizes the display board
 *
 * @return the display board
 * */
char * display_board_init();

/**
 * @fn update_display_board
 * @author Samson Grice
 * @version 1.0
 * @date Mon 30 Mar 2020 13:13:45 UTC
 * @brief a fonction that updates the display board
 *
 * @param display board, game board
 * */
void update_display_board(char * display_board, piece * game_board);

/**
 * @fn show_display_board
 * @author Samson Grice
 * @version 1.0
 * @date Mon 30 Mar 2020 13:20:03 UTC
 * @brief a fonction that displayes the board on the terminal
 *
 * @param display, board
 * */
void show_display_board(char * board);

/**
 * @fn main_game_loop
 * @author Samson Grice
 * @version 1.0
 * @date Sat 04 Apr 2020 18:22:03 UTC
 * @brief a fonction that runs the main game
 *
 * @param the precreated game boards
 * */
void main_game_loop(char * display_board, piece * game_board);

#endif
