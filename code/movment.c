#include "movment.h"

typedef struct{
    int x;
    int y;
}position;



void swap(void * a, void * b, size_t s){
    void * temp = malloc(s);
    memcpy(temp, a, s);
    memcpy(a, b, s);
    memcpy(b, temp, s);
    free(temp);
}
int in(int * array, int size, int x){
    int res = 0;
    int i = 0;
    while (i < size && !res){
        if (array[i] == x){
            res = 1;
        }
        i++;
    }
    return res;
}



/**
 * @fn get_options
 * @author Samson Grice
 * @version 1.0
 * @date Sat 04 Apr 2020 18:49:13 UTC
 * @brief a fonction that returns and prints the move options for that piece
 *
 * @param the letter cooresponding to te piece
 * */
options * get_options(char letter){
    options * opt = malloc(sizeof(options));
    opt->data = malloc(sizeof(int)*8);

    printf("Finding %c\n", letter);

        switch (letter){
        case '+':
            *(opt->data+0) = 8;
            *(opt->data+1) = 6;
            *(opt->data+2) = 2;
            *(opt->data+3) = 4;
            opt->size = 4;
            break;
        case 'Y':
            *(opt->data+0) = 7;
            *(opt->data+1) = 9;
            *(opt->data+2) = 2;
            opt->size = 3;
            break;
        case 'X':
            *(opt->data+0) = 7;
            *(opt->data+1) = 9;
            *(opt->data+2) = 3;
            *(opt->data+3) = 1;
            opt->size = 4;
            break;
        case 'O':
            *(opt->data+0) = 7;
            *(opt->data+1) = 8;
            *(opt->data+2) = 9;
            *(opt->data+3) = 6;
            *(opt->data+4) = 3;
            *(opt->data+5) = 2;
            *(opt->data+6) = 1;
            *(opt->data+7) = 4;
            opt->size = 8;
            break;
        default:
            printf("Wrong letter?\n");
            *(opt->data+0) = -1;
            break;
    }
    opt->data = realloc(opt->data, (opt->size+1)*sizeof(int));
    opt->data[opt->size] = 5;
    opt->size++;
    return opt;
}

/**
 * @fn convert_move_to_coord
 * @author Samson Grice
 * @version 1.0
 * @date Tue 07 Apr 2020 10:46:22 UTC
 * @brief a fonction that takes the move from the imput and converts it into x,y cords
 *
 * @param move
 * */
position convert_move_to_coord(int move){
    int dx, dy;
    switch (move){
        case 7:
            dx = -1;
            dy = -1;
            break;
        case 8:
            dx = 0;
            dy = -1;
            break;
        case 9:
            dx = 1;
            dy = -1;
            break;
        case 4:
            dx = -1;
            dy = 0;
            break;
        case 5:
            dx = 0;
            dy = 0;
            break;
        case 6:
            dx = 1;
            dy = 0;
            break;
        case 1:
            dx = -1;
            dy = 1;
            break;
        case 2:
            dx = 0;
            dy = 1;
            break;
        case 3:
            dx = 1;
            dy = 1;
            break;
        default:
            printf("Wrong number please phone a friend\n\n");
            break;
    }
    position pos;
    pos.x = dx;
    pos.y = dy;

    return pos;
}


/**
 * @fn is_empty
 * @author Samson Grice
 * @version 1.0
 * @date Tue 07 Apr 2020 12:13:09 UTC
 * @brief a fonction that returns true if the square is empty
 *
 * @param game_board x, y
 * */
int is_empty(piece * board, int x, int y){
    return (board + (y*10+x))->player == -1;
}

/**
 * @fn is_edge
 * @author Samson Grice
 * @version 1.0
 * @date Tue 07 Apr 2020 12:43:19 UTC
 * @brief a fonction that returns true if the coords are on a edge piece
 *
 * @param
 * */
int is_edge(int x, int y){
    // goal area needs special case based on player
    return (x == 0 || x == 9 || y == 0 || y == 9);
}

int off_board(int x, int y){
    return (x < 0 || x > 9 || y < 0 || y > 9);
}

int test_jump_move(piece * board, int x, int y, options *  directions, int move);

int * can_jump_away(piece * board, int x, int y, int dx, int dy, options * directions){
    int * res = malloc(directions->size * sizeof(int));
    int i;
    printf("Can jump away?\n");
    position pos;
    for (i=0; i<directions->size;i++){
        pos = convert_move_to_coord(directions->data[i]);
        printf("%d, %d\n", pos.x, pos.y)    ;
        if (!(-dx == pos.x && -dy == pos.y)){
            printf("Different %d %d \n", dx,dy);
            res[i] = test_jump_move(board, x+dx, y+dy, directions, directions->data[i]);
        }
    }
    for (i=0; i<directions->size;i++){
        printf("direction : %d res : %d\n", directions->data[i], res[i]);
    }
    return res;
}

int test_jump_move(piece * board, int x, int y, options *  directions, int move){
    position pos = convert_move_to_coord(move);

    int res = 0;
    /**
     * res = 0 cant move
     * res = 1 move 1 spot
     * res = 3 jump single
     * res = 2 jump multiple
     */


    int dx = pos.x;
    int dy = pos.y;

    printf("Jumping : Pos %d,%d + %d,%d: move: %d\n", x, y, dx, dy, move);

    if (in(directions->data, directions->size, move)){
        printf("Move in directions\n");
        if (!off_board(x+dx,y+dy)){
            printf("Move on the board\n");
            if (is_empty(board, x+dx, y+dy)){
                printf("move is empty\n");
                res = 0;
            }else{
                printf("Move not empty test if can jump\n");
                if (!off_board(x+dx+dx, y+dy+dy)){
                    printf("Jump move on board\n");
                    if (is_empty(board, x+dx+dx,y+dy+dy)){
                        printf("jump move empty\n");
                        if (is_edge(x+dx+dx, y+dy+dy)){
                            printf("jump move is on edge\n");
                            if (in(can_jump_away(board, x+dx, y+dy, dx, dy, directions), directions->size, 2)){
                                printf("jump move on edge but can jump away\n");
                                res = 2;
                            }else{
                                printf("jump move on edge but cannot jump away\n");
                                res = 0;
                            }
                        }else{
                            printf("jump move is not on edge normal + no mutliple jump\n");
                            res = 2;
                        }
                    }else{
                        printf("jump move is not empty cant jump\n");
                        res = 0;
                    }
                }else{
                    printf("jump move not on board, cant jump\n");
                    res = 0;
                }
            }
        }else{
            printf("move not on board\n");
            res = 0;
        }
    }else{
        printf("move not in directions\n");
        res = 0;
    }

    return res;
}


int move_piece(piece * board, int index, int move, int jump){
    position pos = convert_move_to_coord(move);

    int dx = pos.x;
    int dy = pos.y;

    if (jump){
        dx = dx + dx;
        dy = dy + dy;
    }

    swap(board+index, board+(index+(dy*10+dx)), sizeof(piece));
    return (index+(dy*10+dx));
}



int test_move(piece * board, int x, int y, options *  directions, int move, int jumps){
    position pos = convert_move_to_coord(move);

    int res = 0;
    /**
     * res = 0 cant move
     * res = 1 move 1 spot
     * res = 3 jump single
     * res = 2 jump multiple
     */


    int dx = pos.x;
    int dy = pos.y;

    printf("First: Pos %d,%d + %d,%d: move: %d\n", x, y, dx, dy, move);

    if (in(directions->data, directions->size, move)){
        printf("Move in directions\n");
        if (!off_board(x+dx,y+dy)){
            printf("Move on the board\n");
            if (is_empty(board, x+dx, y+dy)){
                printf("move is empty\n");
                if (is_edge(x+dx, y+dy)){
                    printf("Move is on edge\n");
                    if (jumps){
                        printf("Jumped here so can jump away\n");
                        if (in(can_jump_away(board, x, y, dx, dy, directions), directions->size, 2)){
                            printf("Move on edge but can jump away\n");
                            res = 2;
                        }else{
                            printf("Move on edge but cannot jump away\n");
                            res = 0;
                        }
                    }else{
                        printf("Didn't jump here cant jump away\n");
                        res = 0;
                    }

                }else{
                    printf("Move not on egde so normal jump\n");
                    res = 1;
                }
            }else{
                printf("Move not empty test if can jump\n");
                if (!off_board(x+dx+dx, y+dy+dy)){
                    printf("Jump move on board\n");
                    if (is_empty(board, x+dx+dx,y+dy+dy)){
                        printf("jump move empty\n");
                        if (is_edge(x+dx+dx, y+dy+dy)){
                            printf("jump move is on edge\n");
                            if (in(can_jump_away(board, x+dx, y+dy, dx, dy, directions), directions->size, 2)){
                                printf("jump move on edge but can jump away\n");
                                res = 2;
                            }else{
                                printf("jump move on edge but cannot jump away\n");
                                res = 0;
                            }
                        }else{
                            printf("jump move is not on edge normal\n");
                            if (in(can_jump_away(board, x+dx, y+dy, dx, dy, directions), directions->size, 2)){
                                res = 2;
                            }else{
                                res = 3;
                            }
                        }
                    }else{
                        printf("jump move is not empty cant jump\n");
                        res = 0;
                    }
                }else{
                    printf("jump move not on board, cant jump\n");
                    res = 0;
                }
            }
        }else{
            printf("move not on board\n");
            res = 0;
        }
    }else{
        printf("move not in directions\n");
        res = 0;
    }

    if (jumps){
        printf("Jumped here so must jump\n");
        if (res == 1){
            printf("cant do must jump\n");
            res = 0;
        }
    }

    return res;
}
