#include "header.h"

char * display_board_init(){

    char * board = malloc(100 * sizeof(char));
    // malloc test

    int i;
    for (i=0; i<100;i++){
        board[i] = '0';
    }

    return board;
}


void update_display_board(char * display_board, piece * game_board){
    int i;
    for (i=0; i<100;i++){
        if (display_board[i] != game_board[i].letter){
            display_board[i] = game_board[i].letter;
        }
    }
}


void show_display_board(char * board){
    int i, j;
    for (i=0; i<10;i++){
        for (j=0; j<10;j++){
            printf("%c ", board[i*10+j]);
        }
        printf("\n");
    }
}
