#ifndef MOVE_HEADER
#define MOVE_HEADER

#include "header.h"



options * get_options(char letter);

int test_move(piece * board, int x, int y, options * directions, int move, int jumps);

int move_piece(piece * board, int index, int move, int jump);


#endif
