#include "header.h"


/**
 * @fn add_horizonal_pieces
 * @author Samson Grice
 * @version 1.0
 * @date Mon 30 Mar 2020 13:05:49 UTC
 * @brief a fonction that adds the pieces to the starting positions
 *
 * @param the game board, the starting pos (top or bottom), the player
 * */
void add_horizonal_pieces(piece * board, int starting_pos, int player){

    int i;
    for (i=starting_pos; i<starting_pos+8;i++){
        board[i].player = player;
    }

    board[starting_pos].letter = '+';
    board[starting_pos+1].letter = 'Y';
    board[starting_pos+2].letter = 'X';
    board[starting_pos+3].letter = 'O';
    board[starting_pos+4].letter = 'O';
    board[starting_pos+5].letter = 'X';
    board[starting_pos+6].letter = 'Y';
    board[starting_pos+7].letter = '+';

}

piece * game_board_init(){
    int no_players = 2;



    piece * board = malloc(100 * sizeof(piece));
    // malloc test

    piece p;
    p.player = -1;
    p.letter = '0';


    int i;
    for (i=0; i<100;i++){
        board[i] = p;
    }

    if (no_players == 2){
        add_horizonal_pieces(board, 1, 0);
        add_horizonal_pieces(board, 91, 1);
    }

    return board;
}

