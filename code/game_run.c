#include "header.h"
#include "movment.h"


/**
 * @fn choose piece
 * @author Samson Grice
 * @version 1.0
 * @date Sat 04 Apr 2020 18:32:53 UTC
 * @brief a fonction that allows the user to choose a piece
 *
 * @param the players number
 *
 * @return the piece index in board
 * */
int choose_piece(piece * board, int player){
    int x, y;
    int correct = 0;

    do{
        printf("Please enter x coor of piece: ");
        scanf(" %d", &x);
        printf("PLease enter y coor of piece: ");
        scanf(" %d", &y);

        if ((board+(y*10+x))->player == player){
            correct = 1;
        }else{
            printf("That piece isnt yours\nplease try again\n\n");
        }
    }while (!correct);

    return (y*10+x);
}

/**
 * @fn ask_move_piece
 * @author Samson Grice
 * @version 1.0
 * @date Sat 04 Apr 2020 18:47:42 UTC
 * @brief a fonction that will move a piece
 *
 * @param index of piece
 * */
void ask_move_piece(piece * board, char * display,  int index){
    int jumps = 0;
    int res = 0;

    do{
        options * opt = get_options((board+index)->letter);

        int move;
        printf("Where would you like to move? ");
        int i;
        for (i=0; i<opt->size;i++){
            printf("%d  ", *(opt->data+i));
        }
        printf(" : ");
        scanf(" %d", &move);

        printf("%d move\n", move);
        if (move != 5){
            res = test_move(board, index%10, index/10, opt,  move, jumps);

            if (res == 0){
                // bad move try again
            }else{
                if (res == 1){
                    // good move
                    index = move_piece(board, index, move, 0);
                }else{
                    // jump move
                    if (res == 2){
                        jumps++;
                    }
                    index = move_piece(board, index, move, 1);
                }
            }
        }else{
            res = 0;
            printf("ENd of move\n");
        }
        //system("clear");
        printf("\n");
        update_display_board(display, board);
        show_display_board(display);

    }while(res == 0 || res == 2);
}

/**
 * @fn winner
 * @author Samson Grice
 * @version 1.0
 * @date Mon 06 Apr 2020 13:08:46 UTC
 * @brief a fonction that checks if the game is won
 *
 * @param game board
 * */
int winner(piece * board){
    // check top
    int won0 = 1;
    int i = 0;
    while (i < 9 && won0){
        if ((board+i)->player != 1){
            won0 = 0;
        }
        i++;
    }

    // check bottom
    int won1 = 1;
    i = 91;
    while (i < 91 + 8 &&  won1){
        if ((board+i)->player != 0){
            won1 = 0;
        }
        i++;
    }

    return (won0 || won1);
}


void main_game_loop(char * display_board, piece * game_board){
    int no_players = 2;

    int current_player = rand()%no_players;

    int end = 0;

    while (!end){
        printf("Player %d go\n\n", current_player);

        // choose peice
        int piece_index = choose_piece(game_board, current_player);

        printf("piece %c\n", (game_board+piece_index)->letter);
        // move piece

        ask_move_piece(game_board, display_board, piece_index);

        // check win
        if (winner(game_board)){
            end = 1;
        }

        current_player = (current_player+1)%no_players;
        //update_display_board(display_board, game_board);
        //show_display_board(display_board);
    }

    printf("Run succesful\n");
}
